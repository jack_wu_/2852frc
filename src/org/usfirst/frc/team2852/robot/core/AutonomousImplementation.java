package org.usfirst.frc.team2852.robot.core;

/**
 * This class implements AutoCommand and runs the functions to be performed during the 
 * autonomous period.
 * 
 * @author Joel Sauve
 *
 */

public class AutonomousImplementation {
	AutoCommand autoCommander;
	
	public AutonomousImplementation(){
		autoCommander = new AutoCommand();
	}
	
	//contains all auto actions && running constantly during autonomousPeriodic
	public void autonomousRunner(){}
}
