package org.usfirst.frc.team2852.robot.core;

/**
 * This class outlines all autonomous functionality, making use of the original Command
 * structure.
 * 
 * @author Joel Sauve
 *
 */


public class AutoCommand extends Command {
	Hardware hardwareController;
	boolean killSwitch;
	
	public AutoCommand(){
		hardwareController = new Hardware();
		killSwitch = false;
	}
	
	//while loop is recommended here with condition of kill switch == false
	
	//has cube initially
	public void intakeONIFFNoCube(){}
	
	//in case where sensor breaks or doesn't read start cube on auto startup
	public boolean checkInitialState(){return false;}
	
	/**********************AUTO STEPS****************************/
	
	public boolean isAutoLineCrossed(){return false;}
	public boolean isRotateAtAutoLine(){return false;}
	public boolean isContinueForward(){return false;}
	public boolean isRotateParallelToScale(){return false;}
	public boolean isContinueToScale(){return false;}
	public boolean isCubePlaced(){return false;}
	public boolean isFacingPyramidAtAngle(){return false;}
	public boolean isFacingPyramidCardinal(){return false;}
	public boolean isLoweringArmStep(){return false;}
	public boolean isApproachingPyramidAngle(){return false;}
	public boolean isApproachingPyramidPerpend(){return false;}
	public boolean isCubeTaken(){return false;}
	public boolean isInSwitchRange(){return false;}
	public boolean isInSwitchRange2(){return false;} //maybe omit
	
}
