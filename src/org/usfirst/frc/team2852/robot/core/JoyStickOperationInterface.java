package org.usfirst.frc.team2852.robot.core;

/**
 * Interface for JoyStick implementation
 * @author Jack Wu
 *
 */
public interface JoyStickOperationInterface {
	
	void AButtonWhenPressed();
	void BButtonWhenPressed();
	void XButtonWhenPressed();
	void YButtonWhenPressed();
	void startButtonWhenPressed();
	void backButtonWhenPressed();
	void modeButtonWhenPressed();
	void arrowUpButtonWhenPressed();
	void arrowDownButtonWhenPressed();
	void arrowLeftButtonWhenPressed();
	void arrowRightButtonWhenPressed();
	void leftBumperButtonWhenPressed();
	void rightBumperButtonWhenPressed();
	void leftTriggerButtonWhenPressed();
	void rightTriggerButtonWhenPressed();
	void stickWhenMoved(float xAxis, float yAxis);
	
}
