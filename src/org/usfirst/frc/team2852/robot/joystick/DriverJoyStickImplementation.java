package org.usfirst.frc.team2852.robot.joystick;

import org.usfirst.frc.team2852.robot.core.JoyStickOperationInterface;

public class DriverJoyStickImplementation implements JoyStickOperationInterface{
	
	public void AButtonWhenPressed(){}
	public void BButtonWhenPressed(){}
	public void XButtonWhenPressed(){}
	public void YButtonWhenPressed(){}
	public void startButtonWhenPressed(){}
	public void backButtonWhenPressed(){}
	public void modeButtonWhenPressed(){}
	public void arrowUpButtonWhenPressed(){}
	public void arrowDownButtonWhenPressed(){}
	public void arrowLeftButtonWhenPressed(){}
	public void arrowRightButtonWhenPressed(){}
	public void leftBumperButtonWhenPressed(){}
	public void rightBumperButtonWhenPressed(){}
	public void leftTriggerButtonWhenPressed(){}
	public void rightTriggerButtonWhenPressed(){}
	public void stickWhenMoved(float xAxis, float yAxis){}
	
}
