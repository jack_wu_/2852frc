package org.usfirst.frc.team2852.robot.core;

//http://www.ctr-electronics.com/downloads/api/cpp/html/classctre_1_1phoenix_1_1motorcontrol_1_1can_1_1_talon_s_r_x.html#a28528de930e6ee4a817ac6bb5fe97cf8
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.Timer;

/**
 * This class implements a command structure that interfaces directly with the hardware
 * and its functionality.
 * 
 * @author Joel Sauve
 *
 */



public class Command {
	
	private Hardware hardwareController; //interface with hardware class
	private Timer[] array; //array of timers so multiple processes can be ran for specified times 
	
	public Command() {
		hardwareController = new Hardware();
		array = new Timer[100];
	}
	
/*****************DRIVE CODE***************************/
	
	public void driveLeft(double output){}
	
	public void driveRight(double output){}
	
	public void driveForward(double output){}
	
	public void driveBackward(double output){}
	

	
/*********************INTAKE****************************/	
	
	public void intakeIn(double output){}
	
	public void intakeOut(double output){}
	
	public void intakeRotateIn(double output){}

	public void intakeRotateOut(double output){}

/********************ARM**********************************/
	
	public void armUp(double output){}
	
	public void armDown(double output){}
	
/*****************GEAR SHIFT*****************************/
	
	public void gearShift(){}
	
/*****************STOP/KILL FUNCTIONS********************/
	
	public void stopBase(){}
	
	public void stopArm(){}
	
	public void stopIntake(){}
	
	public void stopIntakeRotation(){}
	
	public void killAllSemiAutoCommands(){}
	
/*****************SEMI-AUTO FUNCTIONS********************/
	//NOTE: Java timer recommended for all semi-auto functions to avoid blocked thread
	//all procedures set the lift to specified heights based on encoder values. (parameterized)
	
	public void toGround(){}
	
	public void toSwitch(){}
	
	public void toLowScale(){}
	
	public void toHighScale(){}
	
	public void intakeRotateToAngle(int angle){}
	
	
	
	
	
	
}
