package org.usfirst.frc.team2852.robot.joystick;

import edu.wpi.first.wpilibj.Joystick;

/**
 * 
 * @author Joel Sauve
 *
 */
public class JoyStickObserver {
	
	Joystick driverStick, operatorStick;
	DriverJoyStickImplementation driverJoyStickImplementer;
	OperatorJoyStickImplementation operatorJoyStickImplementer;
	
	//JoyStick Button Mapping
	int AButton = 2;
	int BButton = 3;
	int XButton = 1;
	int YButton = 4;
	int leftBumper = 5;
	int rightBumper = 6;
	int leftTrigger = 7;
	int rightTrigger = 8;
	int backButton = 9;
	int startButton = 10;
	
	int arrowHorizontalAxis = 5;
	int arrowVerticalAxis = 6;
	
	public JoyStickObserver(){
		
		driverStick = new Joystick(0);
		operatorStick = new Joystick(1);
		driverJoyStickImplementer = new DriverJoyStickImplementation();
		operatorJoyStickImplementer = new OperatorJoyStickImplementation();
	
	}

	public void triggerJoyStickImplementation(){ //running constantly in teleopPeriodic
		if(driverStick.getRawButton(AButton)){
			driverJoyStickImplementer.AButtonWhenPressed();
		}
		if(driverStick.getRawButton(BButton)){
			driverJoyStickImplementer.BButtonWhenPressed();
		}
		if(driverStick.getRawButton(XButton)){
			driverJoyStickImplementer.XButtonWhenPressed();
		}
		if(driverStick.getRawButton(YButton)){
			driverJoyStickImplementer.YButtonWhenPressed();
		}
		if(driverStick.getRawButton(leftBumper)){
			driverJoyStickImplementer.leftBumperButtonWhenPressed();
		}
		if(driverStick.getRawButton(rightBumper)){
			driverJoyStickImplementer.rightBumperButtonWhenPressed();
		}
		if(driverStick.getRawButton(leftTrigger)){
			driverJoyStickImplementer.leftTriggerButtonWhenPressed();
		}
		if(driverStick.getRawButton(rightTrigger)){
			driverJoyStickImplementer.rightTriggerButtonWhenPressed();
		}
		if(driverStick.getRawButton(backButton)){
			driverJoyStickImplementer.backButtonWhenPressed();
		}
		if(driverStick.getRawButton(startButton)){
			driverJoyStickImplementer.startButtonWhenPressed();
		}
		driverStick.

		
	}
}

