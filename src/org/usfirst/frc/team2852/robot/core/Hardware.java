package org.usfirst.frc.team2852.robot.core;

// http://www.ctr-electronics.com/downloads/api/cpp/html/classctre_1_1phoenix_1_1motorcontrol_1_1can_1_1_talon_s_r_x.html#a28528de930e6ee4a817ac6bb5fe97cf8
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.BuiltInAccelerometer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.interfaces.Accelerometer;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import com.ctre.phoenix.motorcontrol.ControlMode;


/**
 * This class contains all hardware related Instances & Port Configuration
 * @author Jack Wu
 *
 */
public class Hardware {
	
	// ---  Ports ---
	// Motor Ports
	final int baseLeftMotorPort = 0;
	final int baseRightMotorPort = 0;
	final int intakeLeftMotorPort = 0;
	final int intakeRightMotorPort = 0;
	final int intakeLeftRotationMotorPort = 0;
	final int intakeRightRotationMotorPort = 0;
	final int armLeftRotationMotorPort = 0;
	final int armRightRotationMotorPort = 0;
	
	// Encoder Ports
	final int baseLeftEncoderPortA = 0; final int baseLeftEncoderPortB = 0;
	final int baseRightEncoderPortA = 0; final int baseRightEncoderPortB = 0;
	final int armLeftEncoderPortA = 0; final int armLeftEncoderPortB = 0;
	final int armRightEncoderPortA = 0; final int armRightEncoderPortB = 0;
	
	// Potentiometers Ports
	final int armPotentiometerPort  = 0;
	final int intakePotentiometerPort = 0;
	
	// Solen Ports
	final int intakeLeftSolenPort = 0;
	final int intakeRightSolenPort = 0;
	final int shiftSolenPort = 0;
	
	// Switch Ports
	final int intakeSwitchPort = 0;
	
	// Range Finder Ports
	final int rangeFinderLeftInputPort = 0; final int rangeFinderLeftOutputPort = 0;
	final int rangeFinderRightInputPort = 0; final int rangeFinderRightOutputPort = 0;
	final int rangeFinderFrontInputPort = 0; final int rangeFinderFrontOutputPort = 0;
	final int rangeFinderBackInputPort = 0; final int rangeFinderBackOutputPort = 0;
	
	// Others
	final int gyroPort = 0;
	
	// --- Objects ---
	TalonSRX baseLeftMotor;
	TalonSRX baseRightMotor;
	TalonSRX intakeLeftMotor;
	TalonSRX intakeRightMotor;
	TalonSRX intakeLeftRotationMotor;
	TalonSRX intakeRightRotationMotor;
	TalonSRX armLeftRotationMotor;
	TalonSRX armRightRotationMotor;
	
	Encoder baseLeftEncoder;
	Encoder baseRightEncoder;
	Encoder armLeftEncoder;
	Encoder armRightEncoder;
	
	Solenoid intakeLeftSolen;
	Solenoid intakeRightSolen;
	Solenoid shiftSolen;
	
	Potentiometer armPotentiometer;
	Potentiometer intakePotentiometer;
	
	DigitalInput intakeSwitch;
	
	Ultrasonic rangeFinderLeft;
	Ultrasonic rangeFinderRight;
	Ultrasonic rangeFinderFront;
	Ultrasonic rangeFinderBack;
	
	Gyro gyro1;

	public Hardware(){
		initHardware();
	}
	
	Accelerometer rioAccelerometer;
	
	/**
	 * Init all Hardware Objs
	 */

	public void initHardware() {
		baseLeftMotor = new TalonSRX(baseLeftMotorPort);
		baseRightMotor = new TalonSRX(baseRightMotorPort);
		intakeLeftMotor = new TalonSRX(intakeLeftMotorPort);
		intakeRightMotor = new TalonSRX(intakeRightMotorPort);
		intakeLeftRotationMotor = new TalonSRX(intakeLeftRotationMotorPort);
		intakeRightRotationMotor = new TalonSRX(intakeRightRotationMotorPort);
		armLeftRotationMotor = new TalonSRX(armLeftRotationMotorPort);
		armRightRotationMotor = new TalonSRX(armRightRotationMotorPort);
		
		baseLeftEncoder = new Encoder(baseLeftEncoderPortA, baseLeftEncoderPortB);
		baseRightEncoder = new Encoder(baseRightEncoderPortA, baseRightEncoderPortB);
		armLeftEncoder = new Encoder(armLeftEncoderPortA, armLeftEncoderPortB);
		armRightEncoder = new Encoder(armRightEncoderPortA, armRightEncoderPortB);
		
		intakeLeftSolen = new Solenoid(intakeLeftSolenPort);
		intakeRightSolen = new Solenoid(intakeRightSolenPort);
		shiftSolen = new Solenoid(shiftSolenPort);
		
		armPotentiometer = new AnalogPotentiometer(armPotentiometerPort, 360, 0);
		intakePotentiometer = new AnalogPotentiometer(armPotentiometerPort, 360, 0);
		
		intakeSwitch = new DigitalInput(intakeSwitchPort);
		
		rangeFinderLeft = new Ultrasonic(rangeFinderLeftOutputPort, rangeFinderLeftInputPort);
		rangeFinderRight = new Ultrasonic(rangeFinderRightOutputPort, rangeFinderRightInputPort);
		rangeFinderFront = new Ultrasonic(rangeFinderFrontInputPort, rangeFinderFrontOutputPort);
		rangeFinderBack = new Ultrasonic(rangeFinderBackInputPort, rangeFinderBackOutputPort);
		
		gyro1 = new AnalogGyro(gyroPort);
		rioAccelerometer = new BuiltInAccelerometer(); //8G by default
		
	}
	
	// --- GETTERS ---
	
	/** VERIFICATION IS REQUIRED FOR HIGH & LOW GEAR
	 * 
	 * Read the current value of the solenoid.
	 * @return True if in High gear, False if in Low gear
	 */
	public boolean getCurrentGear() {
		return shiftSolen.get();
	}

	public double getCurrentArmAngle() {
		return armPotentiometer.get();
	}
	
	public double getCurrentIntakeAngle() {
		return intakePotentiometer.get();
	}
	
	/**
	 * get angle from Gyro sensor
	 * @return angle of robot facing
	 */
	public double getAngleError() {
		return gyro1.getAngle();
	}
	
		/**
		 * get X, Y, Z axies acceleration
		 * @return
		 */
		public double getBuiltInAccelerometerPositionX(){
			return rioAccelerometer.getX();
		}
		public double getBuiltInAccelerometerPositionY(){
			return rioAccelerometer.getY();
		}
		public double getBuiltInAccelerometerPositionZ(){
			return rioAccelerometer.getZ();
		}
	
	
	public int getBaseLeftEncoder() {
		return baseLeftEncoder.get();
	}
	public int getBaseRightEncoder() {
		return baseRightEncoder.get();
	}
	
	public int getArmLeftEncoder() {
		return armLeftEncoder.get();
	}
	public int getArmRightEncoder() {
		return armRightEncoder.get();
	}
	
	public boolean isCubeSecured() {
		return intakeSwitch.get();
	}
	
	/**
	 * All Ultrasonic rangerfinder measurement are in MM
	 * @return 
	 */
	public double getRangeFinderLeft() {
		return rangeFinderLeft.getRangeMM();
	}
	public double getRangeFinderRight() {
		return rangeFinderRight.getRangeMM();
	}
	public double getRangeFinderFront() {
		return rangeFinderFront.getRangeMM();
	}
	public double getRangeFinderBack() {
		return rangeFinderBack.getRangeMM();
	}
	
	// --- SETTERS ---
	/**	VEFERICATION IS NEEDED
	 * 
	 * @param highORLow
	 */
	public void setGear(String highORLow) {
		if(highORLow == "high") {
			shiftSolen.set(true);
		}else if(highORLow == "low") {
			shiftSolen.set(false);
		}
	}
	
	public void setBaseLeftMotorOutput(double output) {
		baseLeftMotor.set(ControlMode.PercentOutput, output);
	}
	public void setBaseRightMotorOutput(double output) {
		baseRightMotor.set(ControlMode.PercentOutput, output);
	}
	public void setIntakeLeftMotorOutput(double output) {
		intakeLeftMotor.set(ControlMode.PercentOutput, output);
	}
	public void setIntakeRightMotorOutput(double output) {
		intakeRightMotor.set(ControlMode.PercentOutput, output);
	}
	
	public void setArmLeftRotationOutput(double output) {
		armLeftRotationMotor.set(ControlMode.PercentOutput, output);
	}	
	public void setArmRightRotationOutput(double output) {
		armRightRotationMotor.set(ControlMode.PercentOutput, output);
	}
	public void setIntakeLeftRotationOutput(double output) {
		intakeLeftRotationMotor.set(ControlMode.PercentOutput, output);
	}
	public void setIntakeRightRotationOutput(double output) {
		intakeRightRotationMotor.set(ControlMode.PercentOutput, output);
	}

}
